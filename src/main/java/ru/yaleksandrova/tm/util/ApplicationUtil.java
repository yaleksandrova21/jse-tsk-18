package ru.yaleksandrova.tm.util;

import java.util.Scanner;

public interface ApplicationUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

}
