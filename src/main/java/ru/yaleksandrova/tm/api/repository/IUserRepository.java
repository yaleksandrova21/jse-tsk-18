package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    void clear();

}
