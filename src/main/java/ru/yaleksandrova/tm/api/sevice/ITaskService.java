package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Task;
import java.util.List;
import java.util.Comparator;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll(Comparator<Task> taskComparator);

    void clear();


    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);
}
