package ru.yaleksandrova.tm.api.entity;

import ru.yaleksandrova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
