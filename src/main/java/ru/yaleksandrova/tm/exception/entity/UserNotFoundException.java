package ru.yaleksandrova.tm.exception.entity;

import ru.yaleksandrova.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found");
    }

}
