package ru.yaleksandrova.tm.command.user;

import ru.yaleksandrova.tm.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout user";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
