package ru.yaleksandrova.tm.command.user;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class LoginCommand extends AbstractCommand {

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login user";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = ApplicationUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = ApplicationUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
