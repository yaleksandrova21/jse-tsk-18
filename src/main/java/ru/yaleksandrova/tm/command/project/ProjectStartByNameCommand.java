package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class ProjectStartByNameCommand extends AbstractCommand {
    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by name";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startByName(name);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
    }
}
