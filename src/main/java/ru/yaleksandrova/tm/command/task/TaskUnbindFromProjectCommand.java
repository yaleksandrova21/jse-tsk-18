package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-unbind-from-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER TASK ID]");
        final String taskId = ApplicationUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(taskId);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = ApplicationUtil.nextLine();
        final Task taskUnbind = serviceLocator.getProjectTaskService().unbindTaskById(projectId, taskId);
    }
}
